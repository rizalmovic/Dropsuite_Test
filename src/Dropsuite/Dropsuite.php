<?php

namespace Dropsuite;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * @package Dropsuite
 * @author Habli Muhammad Rizal <rizalmovic@gmail.com>
 */
class Dropsuite
{
    private $_path;
    private $_ignores;
    private $_populate;

    /**
     * Construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->_path = getenv('directory_path');
        $this->_ignores = (getenv('ignore_lists')) ? array_map('trim', explode(',', getenv('ignore_lists'))) : [];
    }

    /**
     * Return path
     *
     * @return string $this->_path
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Return ignore lists
     *
     * @return array $this->_ignores
     */
    public function getIgnoreLists()
    {
        return $this->_ignores;
    }

    /**
     * Get Files
     *
     * @return array
     */
    public function getFiles()
    {
        $files = new RecursiveDirectoryIterator($this->_path);
        $iterator = new RecursiveIteratorIterator($files, RecursiveIteratorIterator::SELF_FIRST);
        $results = [];

        foreach ($iterator as $k => $v) {
            if ($v->isDir() || in_array($v->getFilename(), $this->_ignores)) continue;

            try {
                $md5 = md5_file($v->getPathname());

                if (isset($results[$md5])) {
                    $results[$md5]['count']++;
                } else {
                    $results[$md5] = [
                        'path' => $v->getPathname(),
                        'filename' => $v->getFilename(),
                        'count' => 1,
                    ];
                }
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $results;
    }

    /**
     * Populate files and save results to $_populate
     *
     * @return void
     */
    public function populateIdenticFiles()
    {
        $this->_populate = $this->getFiles();
    }

    /**
     * Get results
     *
     * @return array
     */
    public function getResults()
    {
        $this->populateIdenticFiles();

        if (!count($this->_populate)) {
            return [
                'message' => 'No files found.'
            ];
        }

        $results = $this->_populate;

        foreach ($results as &$r) {
            $r['content'] = file_get_contents($r['path']);
        }

        return $results;
    }
}