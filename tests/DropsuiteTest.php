<?php

use PHPUnit\Framework\TestCase;
use Dropsuite\Dropsuite;

class DropsuiteTest extends TestCase
{
    public $dropsuite;

    public function setUp()
    {
        $dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
        $dotenv->load();
        $this->dropsuite = new Dropsuite();
    }

    public function tearDown()
    {
        $this->dropsuite = null;
    }

    public function log($data)
    {
        fwrite(STDERR, print_r($data, TRUE));;
    }

    public function testPath()
    {
        $this->assertEquals($this->dropsuite->getPath(), getenv('directory_path'));
    }

    public function testIgnoreLists()
    {
        $this->assertEquals(is_array($this->dropsuite->getIgnoreLists()), true);
    }

    public function testGetResults()
    {
        /**
         * Expected results
         */

        $expected = [
            'e80b5017098950fc58aad83c8c14978e' => [
                'path' => './DropsuiteTest/content1',
                'filename' => 'content1',
                'count' => 4,
                'content' => 'abcdef'
            ],
            '9fc9d606912030dca86582ed62595cf7' => [
                'path' => './DropsuiteTest/B/D/diff',
                'filename' => 'diff',
                'count' => 1,
                'content' => 'abcdefghijkl'
            ]
        ];

        $results = $this->dropsuite->getResults();
        $this->assertEquals($results, $expected);
        // $this->log($results); Uncomment this line to see the output on test
    }
}