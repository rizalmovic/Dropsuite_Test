# Dropsuite Test

### Requirements
- PHP 5.6+
- Composer

### Installation
- Run composer install;
- Setup configuration on file `.env`. Copy `.env.example` as `.env`. Sample on configuration is written on `.env.example`;
- Run PHPUnit `./vendor/bin/phpunit`.